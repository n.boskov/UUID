package com.levi9.token;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;

/**
 * Token is at the end of console print.
 *
 */
public class Token 
{
		public static void main( String[] args ) throws UnsupportedEncodingException, NoSuchAlgorithmException
		{
			try {
//				RsaJsonWebKey rsaJsonWebKey = RsaJwkGenerator.generateJwk(512);
//
//				rsaJsonWebKey.setKeyId("k1");

				String secret = "secret";
				int keyByteLength = 32;
				byte [] secretBytes = secret.getBytes("UTF-8");
				int diff = keyByteLength - secretBytes.length;
				byte[] fill = new byte[diff];
				Arrays.fill(fill, (byte)0b00000000);

				byte[] keyBytes = new byte[keyByteLength];
				System.arraycopy(secretBytes, 0, keyBytes, 0, secretBytes.length);
				System.arraycopy(fill, 0, keyBytes, secretBytes.length, fill.length);
				
//				MessageDigest md = MessageDigest.getInstance("SHA-256");
//				String secret = "secret";
//				md.update(secret.getBytes("UTF-8"));
//				byte[] keyBytes = md.digest();
				
				SecretKey key = new SecretKeySpec(keyBytes, AlgorithmIdentifiers.HMAC_SHA256);
				System.out.println("KEY : " + key.getEncoded());
				
//				SecretKey key = new SecretKeySpec(secret.getBytes("UTF-8"), 0, secret.getBytes("UTF-8").length, AlgorithmIdentifiers.HMAC_SHA256);
//				System.out.println("key byte len " + key.getEncoded().length);
				
//				SecretKeySpec key = null;
//				try {
//					key = new SecretKeySpec(("nesto").getBytes("UTF-8"), AlgorithmIdentifiers.HMAC_SHA512);
//					System.out.println("Key length " + key.getEncoded().length);
//				} catch (UnsupportedEncodingException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//					
//				}

//				KeyGenerator kg;
//				SecretKey key = null;
//				try {
//					kg = KeyGenerator.getInstance("AES");
//					kg.init(256);
//					key = kg.generateKey();
//					System.out.println("Key is " + key);
//				} catch (NoSuchAlgorithmException e1) {
//					// TODO Auto-generated catch block
//					System.out.println("KEY not passed");
//					e1.printStackTrace();
//				}
				
				JwtClaims claims = new JwtClaims();
				claims.setIssuer("Issuer");  // who creates the token and signs it
//				claims.setAudience("Audience"); // to whom the token is intended to be sent
				claims.setExpirationTimeMinutesInTheFuture(60); // time when the token will expire (10 minutes from now)
				claims.setGeneratedJwtId(); // a unique identifier for the token
				claims.setIssuedAtToNow();  // when the token was issued/created (now)
				claims.setNotBeforeMinutesInThePast(2); // time before which the token is not yet valid (2 minutes ago)
				claims.setSubject("subject"); // the subject/principal is whom the token is about
				claims.setClaim("email","mail@example.com"); // additional claims/attributes about the subject can be added
				claims.setClaim("username", "user1");
				claims.setClaim("password", "pass1");
				List<String> groups = Arrays.asList("group-one", "other-group", "group-three");
				claims.setStringListClaim("groups", groups); // multi-valued claims work too and will end up as a JSON array
	
				JsonWebSignature jws = new JsonWebSignature();
				jws.setPayload(claims.toJson());
				jws.setKey(key);
//				jws.setKeyIdHeaderValue(rsaJsonWebKey.getKeyId());
				jws.setAlgorithmHeaderValue("HS256");
				String jwt = jws.getCompactSerialization();
	
				System.out.println("JWT token is : " + jwt);
	
				JwtConsumer jwtConsumer = new JwtConsumerBuilder()
						.setRequireExpirationTime() // the JWT must have an expiration time
						.setAllowedClockSkewInSeconds(30) // allow some leeway in validating time based claims to account for clock skew
						.setRequireSubject() // the JWT must have a subject claim
						.setExpectedIssuer("Issuer") // whom the JWT needs to have been issued by
//						.setExpectedAudience("Audience") // to whom the JWT is intended for
						.setVerificationKey(key) // verify the signature with the public keyh
						.build();
				
				JwtClaims jwtClaims;
				try {
					jwtClaims = jwtConsumer.processToClaims(jwt);
					System.out.println("JWT validation succeeded " + jwtClaims);
				} catch (InvalidJwtException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (JoseException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
				System.out.println("Invalid JWT!");
			}
		}

//	public static void main(String[] args) throws Exception {
//		System.out.println(hmacDigest("The quick brown fox jumps over the lazy dog", "key", "HmacSHA256"));
//	}
//
//	public static String hmacDigest(String msg, String keyString, String algo) {
//		String digest = null;
//		try {
//			SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), algo);
//			Mac mac = Mac.getInstance(algo);
//			mac.init(key);
//			
//			byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));
//
//			StringBuffer hash = new StringBuffer();
//			for (int i = 0; i < bytes.length; i++) {
//				String hex = Integer.toHexString(0xFF & bytes[i]);
//				if (hex.length() == 1) {
//					hash.append('0');
//				}
//				hash.append(hex);
//			}
//			digest = hash.toString();
//		} catch (UnsupportedEncodingException e) {
//		} catch (InvalidKeyException e) {
//		} catch (NoSuchAlgorithmException e) {
//		}
//		return digest;
//	}
}
