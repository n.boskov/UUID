package com.levi9.uuid.resources;

import static org.assertj.core.api.Assertions.assertThat;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.ClassRule;
import org.junit.Test;

import io.dropwizard.testing.junit.ResourceTestRule;

public class HelloResourceTest {

	public static final HelloResource helloResource = new HelloResource();

	@ClassRule
	public static final ResourceTestRule resource = ResourceTestRule.builder().addResource(helloResource).build();

	@Test
	public void helloTest() {
		final Response response = resource.client().target("/say-hello").request(MediaType.TEXT_PLAIN_TYPE)
				.get();

		assertThat(response.getStatus()).isEqualTo(200);
	}
	
}
