package com.levi9.uuid.resources;

import static org.assertj.core.api.Assertions.assertThat;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.impl.client.BasicResponseHandler;
import org.junit.ClassRule;
import org.junit.Test;

import io.dropwizard.testing.junit.ResourceTestRule;

public class UUIDResourceTest {

	private static final UUIDResource uuidResource = new UUIDResource();
	
	@ClassRule
	public static final ResourceTestRule resource = ResourceTestRule.builder().addResource(uuidResource).build();
	
	@Test
	public void uuidPostTest() {
		final Response response = resource.client().target("/uuid").request(MediaType.TEXT_PLAIN_TYPE)
				.post(Entity.entity("abc", MediaType.APPLICATION_OCTET_STREAM));
		 
		assertThat(response.getStatus()).isEqualTo(200);
	}
	
	@Test
	public void uuidGetPParamTest() {
		final Response response = resource.client().target("/uuid/abc").request(MediaType.TEXT_PLAIN_TYPE)
				.get();

		assertThat(response.getStatus()).isEqualTo(200);
	}
	
	@Test
	public void uuidGetQParamTest() {
		final Response response = resource.client().target("/uuid").queryParam("array", "abc")
				.request(MediaType.TEXT_PLAIN_TYPE).get();

		assertThat(response.getStatus()).isEqualTo(200);
	}	
	
	@Test
	public void uuidGetManyQParamTest() {
		final Response response = resource.client().target("/uuid/many")
				.queryParam("array", "abcdsa")
				.queryParam("array", "ghadsad")
				.queryParam("array", "dsedasdafewf")
				.queryParam("array", "nnndasq")
				.queryParam("array", "nvndsaq")
				.queryParam("array", "dvnqffasdasdqdwqd")
				.queryParam("array", "gfcss")
				.queryParam("array", "ggasfwfog")
				.queryParam("array", "jufqewfsdr")
				.queryParam("array", "pqfqfel")
				.request(MediaType.TEXT_PLAIN_TYPE).get();

		assertThat(response.getStatus()).isEqualTo(200);
	}	

	
}
