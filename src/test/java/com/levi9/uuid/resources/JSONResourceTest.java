package com.levi9.uuid.resources;

import static org.assertj.core.api.Assertions.assertThat;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.impl.client.BasicResponseHandler;
import org.glassfish.jersey.client.ClientResponse;
import org.junit.ClassRule;
import org.junit.Test;

import io.dropwizard.testing.junit.ResourceTestRule;

public class JSONResourceTest {

	private static final JSONResource jsonResource = new JSONResource("localhost", "6379");
	
	@ClassRule
	public static final ResourceTestRule resource = ResourceTestRule.builder().addResource(jsonResource).build();
	
	@Test
	public void createOrUpdateRecordTest() {
		String json = "{\"fullName\": \"Testi Testio\",\"jobTitle\": \"tester\"}";
		
		final Response response = resource.client().target("/json/someTestUuid").request(MediaType.TEXT_PLAIN)
				.put(Entity.entity(json, MediaType.APPLICATION_JSON));
		 
		assertThat(response.getStatus()).isEqualTo(200);
	}
	
	@Test
	public void getJSONByUUIDTest() {
		final Response response = resource.client().target("/json/someTestUuid").request(MediaType.APPLICATION_JSON)
				.get();

		assertThat(response.getStatus()).isEqualTo(200);		
	}
	
}
