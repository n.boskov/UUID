package com.levi9.uuid.api;

import static org.assertj.core.api.Assertions.assertThat;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.ClassRule;
import org.junit.Test;

import com.levi9.uuid.UUIDApplication;
import com.levi9.uuid.UUIDConfiguration;

import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;

public class UUIDIntegrationTest {

	@ClassRule
	public static final DropwizardAppRule<UUIDConfiguration> RULE = 
		new DropwizardAppRule<UUIDConfiguration>(UUIDApplication.class, ResourceHelpers.resourceFilePath("uuid.yaml"));

	@Test
	public void helloIntegration() {
		Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("hello client");

		Response response = client.target(String.format("http://localhost:%d/say-hello", RULE.getLocalPort()))
				.request()
				.get();

		assertThat(response.getStatus()).isEqualTo(200);
//		assertThat(response.getStatus()).isEqualTo(401);
	}
	
	@Test
	public void uuidPostIntegration() {
		Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("uuid post client");

		Response response = client.target(String.format("http://localhost:%d/uuid", RULE.getLocalPort()))
				.request(MediaType.TEXT_PLAIN_TYPE)
				.post(Entity.entity("abc", MediaType.APPLICATION_OCTET_STREAM));

		assertThat(response.getStatus()).isEqualTo(200);
	}
	
	@Test
	public void uuidGetPParamIntegration() {
		Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("uuid get path params client");

		Response response = client.target(String.format("http://localhost:%d/uuid/abc", RULE.getLocalPort()))
				.request(MediaType.TEXT_PLAIN_TYPE)
				.get();

		assertThat(response.getStatus()).isEqualTo(200);
	}
	
	@Test
	public void uuidGetQParamIntegration() {
		Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("uuid get query param client");

		Response response = client.target(String.format("http://localhost:%d/uuid", RULE.getLocalPort()))
				.queryParam("array", "abc")
				.request(MediaType.TEXT_PLAIN_TYPE).get();

//		assertThat(response.getStatus()).isEqualTo(200);
		assertThat(response.getStatus()).isEqualTo(401);
	}
	
	@Test
	public void uuidGetManyQIntegration() {
		Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("uuid get many query params client");

		Response response = client.target(String.format("http://localhost:%d/uuid/many", RULE.getLocalPort()))
				.queryParam("array", "abcdsa")
				.queryParam("array", "ghadsad")
				.queryParam("array", "dsedasdafewf")
				.queryParam("array", "nnndasq")
				.queryParam("array", "nvndsaq")
				.queryParam("array", "dvnqffasdasdqdwqd")
				.queryParam("array", "gfcss")
				.queryParam("array", "ggasfwfog")
				.queryParam("array", "jufqewfsdr")
				.queryParam("array", "pqfqfel")
				.request(MediaType.TEXT_PLAIN_TYPE).get();

		assertThat(response.getStatus()).isEqualTo(200);
	}
	
}
