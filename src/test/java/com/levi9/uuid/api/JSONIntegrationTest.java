package com.levi9.uuid.api;

import static org.assertj.core.api.Assertions.assertThat;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.ClassRule;
import org.junit.Test;

import com.levi9.uuid.UUIDApplication;
import com.levi9.uuid.UUIDConfiguration;

import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;

public class JSONIntegrationTest {
	
	@ClassRule
	public static final DropwizardAppRule<UUIDConfiguration> RULE = 
	new DropwizardAppRule<UUIDConfiguration>(UUIDApplication.class, ResourceHelpers.resourceFilePath("uuid.yaml"));
	
	@Test
	public void createOrUpdateRecordIntegration() {
		Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("json create or update record client");

		String json = "{\"fullName\": \"Testi Testio\",\"jobTitle\": \"tester\"}";

		Response response = client.target(String.format("http://localhost:%d/json/someTestUuid", RULE.getLocalPort()))
				.request(MediaType.TEXT_PLAIN)
				.put(Entity.entity(json, MediaType.APPLICATION_JSON));
	}

	@Test
	public void getJSONByUUIDIntegration() {
		Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("get json by uuid client");

		final Response response = client.target(String.format("http://localhost:%d/json/someTestUuid", RULE.getLocalPort()))
				.request(MediaType.APPLICATION_JSON)
				.get();

		assertThat(response.getStatus()).isEqualTo(200);
	}

}
