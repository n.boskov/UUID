package com.levi9.uuid;

import io.dropwizard.Configuration;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UUIDConfiguration extends Configuration {
	@NotEmpty
	private String redisHost;

	@NotEmpty
	private String redisPort;

	@NotEmpty
	private String zabbixServer;
	
	@NotEmpty
	private String zabbixReporterPeriodBetweenPollsInSeconds;

	@JsonProperty
	public String getRedisHost() {
		return redisHost;
	}

	@JsonProperty
	public String setRedisHost(String redisHost) {
		return this.redisHost = redisHost;
	}

	@JsonProperty
	public String getRedisPort() {
		return redisPort;
	}

	@JsonProperty
	public void setRedisPort(String name) {
		this.redisPort = name;
	}
	
	@JsonProperty
	public String getZabbixServer() {
		return zabbixServer;
	}

	@JsonProperty
	public void setZabbixServer(String zabbixServer) {
		this.zabbixServer = zabbixServer;
	}
	
	@JsonProperty
	public String getZabbixReporterPeriodBetweenPollsInSeconds() {
		return zabbixReporterPeriodBetweenPollsInSeconds;
	}

	@JsonProperty
	public void setZabbixReporterPeriodBetweenPollsInSeconds(
			String zabbixReporterPeriodBetweenPollsInSeconds) {
		this.zabbixReporterPeriodBetweenPollsInSeconds = zabbixReporterPeriodBetweenPollsInSeconds;
	}
}
