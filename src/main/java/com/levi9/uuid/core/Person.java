package com.levi9.uuid.core;

import java.util.Objects;

public class Person {
	
	private String fullName;

	private String jobTitle;

	public Person() {
	}

	public Person(String fullName, String jobTitle) {
		this.fullName = fullName;
		this.jobTitle = jobTitle;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Person)) {
			return false;
		}

		final Person that = (Person) o;

		return  Objects.equals(this.fullName, that.fullName) &&
				Objects.equals(this.jobTitle, that.jobTitle);
	}

	@Override
	public String toString() {
		return "Person [fullName=" + fullName + ", jobTitle=" + jobTitle + "]";
	}
	
}

