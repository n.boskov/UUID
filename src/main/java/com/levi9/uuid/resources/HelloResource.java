package com.levi9.uuid.resources;

import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;
import com.levi9.auth.jwt.User;

import io.dropwizard.auth.Auth;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/say-hello")
@Produces(MediaType.TEXT_PLAIN)
public class HelloResource {

	@GET
	@Timed
	public String sayHello() {
		return "hello!";
	}
}
