package com.levi9.uuid.resources;

import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.common.base.Optional;
import com.levi9.auth.jwt.User;
import com.levi9.uuid.UUIDApplication;
import com.levi9.uuid.UUIDConfiguration;
import com.levi9.uuid.core.Person;

import io.dropwizard.auth.Auth;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.print.attribute.ResolutionSyntax;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/json")
public class JSONResource {
	
	String redisHost;
	String redisPort;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UUIDResource.class);
	
	public JSONResource(String host, String port) {
		this.redisHost = host;
		this.redisPort = port;
	}
		
	@PUT
	@Path("{uuid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Timed
	public String createOrUpdateRecord(@PathParam("uuid") String uuid, Person person) throws IllegalArgumentException, SecurityException {
		
		try (Jedis jedis = UUIDApplication.jedisPool.getResource()) {
			
			Map<String, String> json = new HashMap<String, String>();
			for (Field field : person.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				try {
					json.put(field.getName(), field.get(person).toString());
				} catch (IllegalAccessException e) {
					LOGGER.error("There is no field : " + field.getName() + " in Person class.");
					e.printStackTrace();				
				}
			}
			jedis.hmset(uuid, json);
		}
		
		return "OK";
	
	}
	
	@GET
	@Path("{uuid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	@Timed
	public Person getJSONByUUID(@PathParam("uuid") String uuid) {

		Person reconstructedJson = new Person();

		try (Jedis jedis = UUIDApplication.jedisPool.getResource()) {

			if (jedis.hgetAll(uuid).entrySet().size() == 0) throw new NotFoundException();

			for (Map.Entry<String, String> entry : jedis.hgetAll(uuid).entrySet()) {
				Field field = null;
				try {
					field = reconstructedJson.getClass().getDeclaredField(entry.getKey());
				} catch (NoSuchFieldException e) {
					LOGGER.error("There is no field : " + entry.getKey() + " in Person class.");
					e.printStackTrace();
				} catch (SecurityException e) {
					e.printStackTrace();
				}
				field.setAccessible(true);
				try {
					field.set(reconstructedJson, entry.getValue());
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}

		return reconstructedJson;

	}
}
