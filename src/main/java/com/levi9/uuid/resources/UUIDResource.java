package com.levi9.uuid.resources;

import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.google.common.base.Optional;
import com.levi9.auth.jwt.User;
import com.levi9.uuid.UUIDConfiguration;
import com.levi9.uuid.core.Person;

import io.dropwizard.auth.Auth;
import redis.clients.jedis.Jedis;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.print.attribute.ResolutionSyntax;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/uuid")
@Consumes(MediaType.APPLICATION_OCTET_STREAM)
@Produces(MediaType.TEXT_PLAIN)
public class UUIDResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(UUIDResource.class);
	
	public UUIDResource() {
	}
	
	@POST
	@Timed
	public String postUUID(InputStream stream) {
		byte[] byteArray;
		try {
			byteArray = IOUtils.toByteArray(stream);
			return UUID.nameUUIDFromBytes(byteArray).toString();
		} catch (IOException e) {
			e.printStackTrace();
			return "Error generating UUID from bytes.";
		}
	}
		
	@GET
	@Path("{array}")
	@Timed
	public String getUUIDPParam(@PathParam("array") String array) {
		byte[] byteArray = array.getBytes(Charset.forName("UTF-8"));
		return UUID.nameUUIDFromBytes(byteArray).toString();
	}
	
	@GET
	@Timed
	public String getUUIDQParam(@QueryParam("array") String array, @Auth User user) {
		byte[] byteArray = array.getBytes(Charset.forName("UTF-8"));
		return UUID.nameUUIDFromBytes(byteArray).toString();		
	}
	
	@GET
	@Path("/many")
	@Timed
	public String getUUIDManyQParam(@QueryParam("array") final List<String> arrays) {
		return buildCommaSeparatedUUIDs(arrays);
	}
		
	private String buildCommaSeparatedUUIDs(List<String> arrays) {
		StringBuilder sb = new StringBuilder();
		ArrayList<String> uuidsS = new ArrayList<String>();

		Iterator arraysI = arrays.iterator();
		while(arraysI.hasNext()) {
			byte[] byteArray = ((String) arraysI.next()).getBytes(Charset.forName("UTF-8"));
			uuidsS.add(UUID.nameUUIDFromBytes(byteArray).toString());			
		}

		for(String string : uuidsS) {
			sb.append(string);
			sb.append(",");
		}
		return sb.length() > 0 ? sb.substring(0, sb.length() - 1): "";
	}
	
}
