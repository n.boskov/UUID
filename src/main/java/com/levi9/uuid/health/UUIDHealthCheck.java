package com.levi9.uuid.health;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.health.HealthCheck;

import redis.clients.jedis.Jedis;

public class UUIDHealthCheck extends HealthCheck {

	private static final Logger LOGGER = LoggerFactory.getLogger(UUIDHealthCheck.class);
	
	String jedisHost;
	int jedisPort;

	public UUIDHealthCheck(String jedisHost, int jedisPort) {
		super();
		this.jedisHost = jedisHost;
		this.jedisPort = jedisPort;
	}

	@Override
	protected Result check() throws Exception {
		try { 
			(new Jedis(jedisHost, jedisPort)).ping();
			return Result.healthy();
		} catch (Exception e) {
			LOGGER.error("Redis is unreachable on -> " + this.jedisHost + ":" + this.jedisPort);
			return Result.unhealthy("Redis is unreachable on -> " + this.jedisHost + ":" + this.jedisPort);
		}

	}

}
