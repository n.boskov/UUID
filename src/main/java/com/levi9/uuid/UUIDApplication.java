package com.levi9.uuid;

import io.dropwizard.Application;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.annotation.Timed;
import com.levi9.auth.jwt.JWTAuthenticator;
import com.levi9.auth.jwt.JWTAuthorizer;
import com.levi9.auth.jwt.JWTCredentialAuthFilter;
import com.levi9.auth.jwt.User;
import com.levi9.metrics.zabbix.ZabbixReporter;
import com.levi9.metrics.zabbix.ZabbixSender;
import com.levi9.uuid.health.UUIDHealthCheck;
import com.levi9.uuid.resources.HelloResource;
import com.levi9.uuid.resources.JSONResource;
import com.levi9.uuid.resources.UUIDResource;

public class UUIDApplication extends Application<UUIDConfiguration> {

	private static final Logger LOGGER = LoggerFactory.getLogger(UUIDApplication.class);	
	public static JedisPool jedisPool;
	
    public static void main(final String[] args) throws Exception {
        new UUIDApplication().run(args);
    }

    @Override
    public String getName() {
        return "UUID";
    }

    @Override
    public void initialize(final Bootstrap<UUIDConfiguration> bootstrap) {
    }

    @Override
    public void run(final UUIDConfiguration configuration,
                    final Environment environment) {
    	jedisPool =  new JedisPool(configuration.getRedisHost(), Integer.parseInt(configuration.getRedisPort()));
    	LOGGER.info("Jedis pool is established at : " + configuration.getRedisHost() + ":" + configuration.getRedisPort());
    	LOGGER.info("Jedis active connections: " + jedisPool.getNumActive() + 
    				"/nJedis idle connections: " + jedisPool.getNumIdle());

    	List<String> discoveryNames = new ArrayList<String>();
    	
    	final HelloResource helloResource = new HelloResource();
    	final UUIDResource uuidPResource = new UUIDResource();
    	final JSONResource jsonResource = new JSONResource(configuration.getRedisHost(), configuration.getRedisPort());
    	
    	final UUIDHealthCheck uuidHealthCheck = new UUIDHealthCheck(configuration.getRedisHost(),
    																	Integer.parseInt(configuration.getRedisPort()));
    	
    	environment.jersey().register(new AuthDynamicFeature((new JWTCredentialAuthFilter.Builder<User>()).setPrefix("Bearer")
    			.setAuthenticator(new JWTAuthenticator())
    			.setAuthorizer(new JWTAuthorizer())
    			.buildAuthFilter()));
    	environment.jersey().register(helloResource);
    	discoveryNames.addAll(getResourceDiscoveryNames(helloResource));
    	environment.jersey().register(uuidPResource);
    	discoveryNames.addAll(getResourceDiscoveryNames(uuidPResource));
    	environment.jersey().register(jsonResource);
    	discoveryNames.addAll(getResourceDiscoveryNames(jsonResource));
    	
    	environment.healthChecks().register("uuidHealthCheck", uuidHealthCheck);
    
    	//Metrics
    	String hostName;
		try {
			hostName = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			LOGGER.info("Error in detecting the hostname: ", e);
			throw new RuntimeException(e);
		}
    	LOGGER.info("Zabbix Hostname: " + hostName);
    	LOGGER.info("Zabbix server: " + configuration.getZabbixServer());
    	ZabbixSender zabbixSender = new ZabbixSender(configuration.getZabbixServer(), hostName);
     
    	// Teach Zabbix over Discovery
		String applicationIdentifier = getName(); 
		zabbixSender.sendJsonForDiscovery(discoveryNames, applicationIdentifier, "resource-metrics-discovery", "{#TIMER}");
		zabbixSender.sendJsonForDiscovery(Arrays.asList("jvm"), applicationIdentifier, "jvm-metrics-discovery", "{#TIMER}");
		zabbixSender.sendJsonForDiscovery(Arrays.asList("io.dropwizard.jetty.MutableServletContextHandler"), applicationIdentifier, "mutable-servlet-context-handler", "{#UUID}");
		LOGGER.info("Jsons for discovery sent.");
		
		//Zabbix reporter
		ZabbixReporter zabbixReporter = new ZabbixReporter(environment.metrics(), "", TimeUnit.SECONDS, TimeUnit.MILLISECONDS, MetricFilter.ALL, getName() ,zabbixSender);
		zabbixReporter.start(new Long(configuration.getZabbixReporterPeriodBetweenPollsInSeconds()), TimeUnit.SECONDS);

    }
    
    private List<String> getResourceDiscoveryNames(Object resource) {
    	List<String> discoveryNames = new ArrayList<String>();
		@SuppressWarnings("unchecked")
		Class<? extends Object> clazz = resource.getClass();
		String clazzName = clazz.getName();

		Method[] methods = clazz.getMethods();
		for (Method method : methods) {
			if (method.isAnnotationPresent((Class<? extends Annotation>) Timed.class)) {
				if (method.isAnnotationPresent((Class<? extends Annotation>) GET.class) ||
						method.isAnnotationPresent((Class<? extends Annotation>) POST.class) ||
						method.isAnnotationPresent((Class<? extends Annotation>) PUT.class) ||
						method.isAnnotationPresent((Class<? extends Annotation>) DELETE.class)) {
					discoveryNames.add(clazzName + "." + method.getName());
				}
			}
		}
		
		return discoveryNames;
	}
}
