package com.levi9.auth.jwt;

import java.security.Principal;

public class User implements Principal {
    private final String name;
    private final String password;

    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }
    
    public String getPassword() {
    	return password;
    }
}
