package com.levi9.auth.jwt;

import javax.ws.rs.core.SecurityContext;

public interface JWTSecurityContext extends SecurityContext {
	/**
	 * String identifier for JWT authentication. Value "JWT"
	 */
	public static final String JWT_AUTH = "JWT";

}
