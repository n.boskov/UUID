package com.levi9.auth.jwt;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.HmacUsingShaAlgorithm.HmacSha256;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.lang.JoseException;

import com.google.common.base.Optional;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;

public class JWTAuthenticator implements Authenticator<String, User> {
    
	@Override
	public Optional<User> authenticate(String JWTcredentials) throws AuthenticationException {

		try {
			
			String secret = "secret";
			int keyByteLength = 32;
			byte [] secretBytes = secret.getBytes("UTF-8");
			int diff = keyByteLength - secretBytes.length;
			byte[] fill = new byte[diff];
			Arrays.fill(fill, (byte)0b00000000);

			byte[] keyBytes = new byte[keyByteLength];
			System.arraycopy(secretBytes, 0, keyBytes, 0, secretBytes.length);
			System.arraycopy(fill, 0, keyBytes, secretBytes.length, fill.length);

			SecretKey key = new SecretKeySpec(keyBytes, AlgorithmIdentifiers.HMAC_SHA256);
						
			JwtConsumer jwtConsumer = new JwtConsumerBuilder()
					.setVerificationKey(key)
					.build();					
			
			JwtClaims claims = jwtConsumer.processToClaims(JWTcredentials);
			String username = claims.getStringClaimValue("username");
			String password = claims.getStringClaimValue("password");
			return Optional.of(new User(username, password));
			
		} catch (InvalidJwtException e) {
			e.printStackTrace();
			return Optional.absent();
		} catch (MalformedClaimException e) {
			e.printStackTrace();
			return Optional.absent();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return Optional.absent();
		}
	}

}
