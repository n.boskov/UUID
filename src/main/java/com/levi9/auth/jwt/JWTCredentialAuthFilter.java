package com.levi9.auth.jwt;

import com.google.common.base.Optional;
import com.google.common.io.BaseEncoding;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.AuthFilter;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.Authorizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Priority;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Priorities;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.SecurityContext;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.Principal;

@Priority(Priorities.AUTHENTICATION)
public class JWTCredentialAuthFilter<P extends Principal> extends AuthFilter<String, P> {
	private static final Logger LOGGER = LoggerFactory.getLogger(JWTCredentialAuthFilter.class);

	private JWTCredentialAuthFilter() {
	}

	@Override
	public void filter(final ContainerRequestContext requestContext) throws IOException {
		final String header = requestContext.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
		try {
			if (header != null) {
				final int space = header.indexOf(' ');
				if (space > 0) {
					final String method = header.substring(0, space);
					if (prefix.equalsIgnoreCase(method)) {
						final String rawToken = header.substring(space + 1);
						try {
							final Optional<P> principal = authenticator.authenticate(rawToken);
							if (principal.isPresent()) {
								requestContext.setSecurityContext(new SecurityContext() {

									@Override
									public boolean isUserInRole(String role) {
										return authorizer.authorize(principal.get(), role);
									}

									@Override
									public boolean isSecure() {
										return requestContext.getSecurityContext().isSecure();									
									}

									@Override
									public Principal getUserPrincipal() {
										return principal.get();
									}

									@Override
									public String getAuthenticationScheme() {
										return JWTSecurityContext.JWT_AUTH;
									}
								});
								return;
							}
						} catch (Exception e) {
							LOGGER.warn("Error authenticating credentials", e);
							throw new InternalServerErrorException();
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Error authenticating credentials", e);
			throw new InternalServerErrorException();
		}

		throw new WebApplicationException(unauthorizedHandler.buildResponse(prefix, realm));
	}


	/**
	 * Builder for {@link JWTCredentialAuthFilter}.
	 * <p>An {@link Authenticator} must be provided during the building process.</p>
	 *
	 * @param <P> the principal
	 */
	public static class Builder<P extends Principal> extends
	AuthFilterBuilder<String, P, JWTCredentialAuthFilter<P>> {
		@Override
		protected JWTCredentialAuthFilter<P> newInstance() {
			return new JWTCredentialAuthFilter<>();
		}
	}
}
