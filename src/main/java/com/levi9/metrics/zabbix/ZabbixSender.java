package com.levi9.metrics.zabbix;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZabbixSender {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	public static final int DEFAULT_ZABBIX_SERVER_PORT = 10051;

	private String zabbixServer;
	private String host;

	private String keyPrefix = "";

	public ZabbixSender(String zabbixServer, String host) {
		this.zabbixServer = zabbixServer;
		if(host != null) {
			this.host = host;
		} else {
			this.host = getHostName();
		}
	}

	public void sendMetric(Map<String, Object> map) {
		try {
			String toSend = convertToJson(map);
			sendJson(toSend);
		} catch (Exception ex) {
			LOGGER.error("Zabbix data not sent. Exception : {}", ex.toString() + " " + Arrays.toString(ex.getStackTrace()));
		}
	}

	private void sendJson(String toSend) throws IOException {
		Socket socket = null;
		BufferedReader br = null; 
		
		try {
			socket = new Socket(zabbixServer, DEFAULT_ZABBIX_SERVER_PORT);
			OutputStream os = socket.getOutputStream();
			br = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			LOGGER.debug("ZabbixSender data Sent (len:" + toSend.length() + ") [to:" + zabbixServer + "] : data : " + toSend.replace("\n", " "));
	
			sendZabbixMessage(os, toSend.getBytes());
	
			String response = br.readLine();
			String bufferLine;
			while ((bufferLine = br.readLine()) != null) {
				response += bufferLine;
			}
			//failed: 0
			if(response.contains("failed: 0")) {
				LOGGER.debug("ZabbixSender data response Received [from:" + zabbixServer + "] : " + response.replace("\n", " "));
			} else {
				LOGGER.error("ZabbixSender data response Received [from:" + zabbixServer + "] : " + response.replace("\n", " ") + "\n" + toSend);
			}
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch (Exception e) {
					LOGGER.error("ZabbixSender Buffered Reader close exception: ", e);
				}
			}
			if(socket != null) {
				try {
					socket.close();
				} catch (Exception e) {
					LOGGER.error("ZabbixSender socket close exception: ", e);
				}				
			}
			
		}
		
	}

	private String convertToJson(Map<String, Object> values) {
		StringBuilder sb = new StringBuilder(4096);

		sb.append(addJsonBeginningPart());

		boolean more = false;
		for (Map.Entry<String, Object> entry : values.entrySet()) {
			if (more) {
				sb.append(", \n");
			}
			sb.append("{");
			valuePair(sb, "host", host);
			sb.append(',');
			valuePair(sb, "key", keyPrefix + entry.getKey());
			sb.append(',');
			valuePair(sb, "value", entry.getValue().toString());
			sb.append("}");
			more = true;
		}
		sb.append("]\n");
		sb.append("}");

		return sb.toString();
	}

	public void sendJsonForDiscovery(List<String> names, String applicationIdentifier, String key, String zabbixPattern) {
		try {
			String toSend = makeJsonForDiscovery(names, applicationIdentifier, key, zabbixPattern);
			sendJson(toSend);
		} catch (Exception e) {
			LOGGER.error("Failed to send zabbix data. Exception : {}", e);
		}
	}

	private StringBuilder addJsonBeginningPart() {
		StringBuilder sb = new StringBuilder(4096);
		sb.append("{");
		valuePair(sb, "request", "sender data");
		sb.append(", ");
		quote(sb, "data").append(": [");

		return sb;
	}

	private String makeJsonForDiscovery(List<String> names, String applicationIdentifier, String key, String zabbixPattern) {
		StringBuilder sb = new StringBuilder(4096);
		sb.append(addJsonBeginningPart());

		sb.append("{");
		valuePair(sb, "host", host);
		sb.append(", ");
		valuePair(sb, "key", key);
		sb.append(", ");
		quote(sb, "value").append(": ");
		quoteLeftOnly(sb, "{");
		quoteEscaped(sb, "data").append(": [");

		boolean more = false;
		for (String name : names) {
			if (more) {
				sb.append(", ");
			}
			sb.append("{");
			valuePairEscapedQuotes(sb, "{#APPID}", applicationIdentifier);
			sb.append(", ");
			valuePairEscapedQuotes(sb, zabbixPattern, name);
			sb.append("}");
			more = true;
		}

		sb.append("]");
		quoteRightOnly(sb, "}");
		sb.append("}");
		sb.append("]");
		sb.append("}");

		return sb.toString();
	}

	private void valuePairEscapedQuotes(StringBuilder sb, String key, String value) {
		sb.append("\\").append('"').append(key).append("\\\": \\\"").append(value).append("\\\"");
	}

	private StringBuilder quoteEscaped(StringBuilder sb, String text) {
		return sb.append("\\").append('"').append(text).append("\\").append('"');
	}

	private StringBuilder quoteLeftOnly(StringBuilder sb, String text) {
		return sb.append('"').append(text);
	}

	private StringBuilder quoteRightOnly(StringBuilder sb, String text) {
		return sb.append(text).append('"');
	}

	private void valuePair(StringBuilder sb, String key, String value) {
		sb.append('"').append(key).append("\": \"").append(value).append("\"");
	}

	private StringBuilder quote(StringBuilder sb, String text) {
		return sb.append('"').append(text).append('"');
	}

	private void sendZabbixMessage(OutputStream out, byte[] data) throws IOException {
		int length = data.length;

		out.write(new byte[] { 'Z', 'B', 'X', 'D', '\1', (byte) (length & 0xFF), (byte) ((length >> 8) & 0x00FF), (byte) ((length >> 16) & 0x0000FF), (byte) ((length >> 24) & 0x000000FF), '\0', '\0',
				'\0', '\0' });
		out.write(data);
	}

	public String getHostName() {
		try {
			String hostName = InetAddress.getLocalHost().getHostName();
			if (hostName != null && !hostName.isEmpty()) {
				return hostName;
			}
		} catch (UnknownHostException e) {
			LOGGER.error("get hostName error!", e);
		}
		return null;
	}
}
