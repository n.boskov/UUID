package com.levi9.metrics.zabbix;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.Metered;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.ScheduledReporter;
import com.codahale.metrics.Snapshot;
import com.codahale.metrics.Timer;

public class ZabbixReporter extends ScheduledReporter {

	private static final Logger LOGGER = LoggerFactory.getLogger(ZabbixReporter.class);
	
	private ZabbixSender zabbixSender;
	private String applicationIdentifier;
	
	public ZabbixReporter(MetricRegistry registry, String name, TimeUnit rateUnit, TimeUnit durationUnit, MetricFilter filter, String applicationIdentifier, ZabbixSender zabbixSender) {
		super(registry, name, filter, rateUnit, durationUnit);
		this.zabbixSender = zabbixSender;
		this.applicationIdentifier = applicationIdentifier;
	}

	//Discovery keys
	private void addDiscoverySnapshotMetricsWithConvertDuration(String key, Snapshot snapshot, Map<String, Object> metrics) {
		metrics.put("timer.min[" + applicationIdentifier + "," + key + "]", convertDuration(snapshot.getMin()));
		metrics.put("timer.max[" + applicationIdentifier + "," + key + "]", convertDuration(snapshot.getMax()));
		metrics.put("timer.mean[" + applicationIdentifier + "," + key + "]", convertDuration(snapshot.getMean()));
	}
	
	private void addDiscoveryMeterMetrics(String key, Metered meter, Map<String, Object> metrics) {
		metrics.put("timer.count[" + applicationIdentifier + "," + key + "]", meter.getCount());
	}
	
	private void addSnapshotMetricsWithConvertDuration(String key, Snapshot snapshot, Map<String, Object> metrics) {
		metrics.put(appendApplicationIdentifier(key + ".min"), convertDuration(snapshot.getMin()));
		metrics.put(appendApplicationIdentifier(key + ".max"), convertDuration(snapshot.getMax()));
		metrics.put(appendApplicationIdentifier(key + ".mean"), convertDuration(snapshot.getMean()));
	}
	
	private void addMeterMetrics(String key, Metered meter, Map<String, Object> metrics) {
		metrics.put(appendApplicationIdentifier(key + ".count"), meter.getCount());
	}
	
	private String appendApplicationIdentifier(String key) {
		return key + "[" + applicationIdentifier + "]";
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void report(SortedMap<String, Gauge> gauges, SortedMap<String, Counter> counters, SortedMap<String, Histogram> histograms, SortedMap<String, Meter> meters, SortedMap<String, Timer> timers) {
		Map<String, Object> metrics = new HashMap<String, Object>();
		
		for (Map.Entry<String, Gauge> entry : gauges.entrySet()) {
			metrics.put(appendApplicationIdentifier(entry.getKey()), entry.getValue().getValue());
		}
		
		for (Map.Entry<String, Counter> entry : counters.entrySet()) {
			metrics.put(appendApplicationIdentifier(entry.getKey()), entry.getValue().getCount());
		}
		for (Map.Entry<String, Histogram> entry : histograms.entrySet()) {
			Histogram histogram = entry.getValue();
			long count = histogram.getCount();
			Snapshot snapshot = histogram.getSnapshot();
			addSnapshotMetricsWithConvertDuration(entry.getKey(), snapshot, metrics);
		}
		for (Map.Entry<String, Meter> entry : meters.entrySet()) {
			Meter meter = entry.getValue();
			addMeterMetrics(entry.getKey(), meter, metrics);
		}
		
		for (Map.Entry<String, Timer> entry : timers.entrySet()) {
			Timer timer = entry.getValue();
			String key = entry.getKey();
			if(key.startsWith("com.levi9")) {
				addDiscoveryMeterMetrics(entry.getKey(), timer, metrics);
				addDiscoverySnapshotMetricsWithConvertDuration(entry.getKey(), timer.getSnapshot(), metrics);
			} else {
				addMeterMetrics(entry.getKey(), timer, metrics);
				addSnapshotMetricsWithConvertDuration(entry.getKey(), timer.getSnapshot(), metrics);
			}
		}
		zabbixSender.sendMetric(metrics);
	}
}